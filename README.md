# YPBCW - Your/YAML Proxmox-Backup-Client Wrapper

Making backup to a proxmox backup server more simple

## Status / Objectives

This is an experiment to make some tooling around [proxmox-backup-client](https://pbs.proxmox.com/docs/backup-client.html). 

The objective is to have a wrapper that can be configured with a simple *yaml* file to do simple operations like creating a backup, listing them etc.


## Usage



## TODO

- Playing pre/post backup/archive hooks
- Encryption / Signature


