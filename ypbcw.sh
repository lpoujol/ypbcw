#!/bin/bash

# ypbcw - Your/YAML proxmox-backup-client wrapper
# Copyright (C) 2024 Ludovic Poujol <lpoujol@posteo.net>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.


## Configuration parsion / Value extraction
###########################################

function get_destination() {
  local destination_name=$1

  echo "${FULL_JSON_CONFIG}" | ${JQ_BIN} -c ".destinations.${destination_name}"
}

function get_destination_attr() {
  local destination_name=$1
  local destination_attribute=$2

  echo "${FULL_JSON_CONFIG}" | ${JQ_BIN} --raw-output ".destinations.${destination_name}.${destination_attribute}"
}

function get_destination_list() {
  echo "${FULL_JSON_CONFIG}" | ${JQ_BIN} --raw-output ".destinations | keys[]"
}

function get_archives(){
  echo "${FULL_JSON_CONFIG}" | ${JQ_BIN} -c ".archives[]"
}

function get_archive_attr(){
  local archive=$1
  local archive_attribute=$2

  echo "${archive}" | ${JQ_BIN} --raw-output ".${archive_attribute}"
}

function get_attr(){
  local attribute=$1

  echo "${FULL_JSON_CONFIG}" | ${JQ_BIN} --raw-output ".${attribute}"
}

## Logging / Output
###########################################

function _log() {
  if [ -x "${LOGGER_BIN}" ]; then
    ${LOGGER_BIN} -t "pbcw" -- "$1"
  fi
}

function log_error() {
  echo "$1" >&2
  _log "$1"
}

function log_message() {
  if [[ "${verbose}" = "1" ]]; then
    echo "$1" >&2
  fi
  _log "$1"
}

function log_message_spacer() {
  log_message "----------------------------------------------------"
}

## Help / Usage
###########################################

function usage() { 
  cat <<UNLIKELY_EOF
Usage $0 [-c /path/to/config.yml] [-a (backup|list|...)] [-v]
See $0 -h for more details
UNLIKELY_EOF

  exit 0
}

function help(){
  cat <<UNLIKELY_EOF
ypbcw : Your/YAML proxmox-backup-client wrapper

Usage $0
  or  $0 -a list
  or  $0 -c /path/to/config.yml -v -a backup
  or  $0 -c /path/to/config.yml -v -a list
  or  $0 -c /path/to/config.yml -a command [backup_dest]


Options
-a ACTION               Select the action to do.
                        Possible values are : 
                         - backup > Do the backups
                         - list > List archives in all datastores configured (filtered with the current hostname)
                         - command > Return proxmox-backup-client command with parameters 
                                     The last argument given to the command will be used a filter to display only one command
                        Default value is "backup"
-c /path/to/config.yml  Use the specified config file
-v                      Increase verbosity
-h                      This help message

UNLIKELY_EOF
  exit 0
}

## Requirements / Dependencies check
###########################################

function check_requirements(){

  if ! [[ -x $(command -v proxmox-backup-client) ]]; then
    log_error "FATAL : Missing proxmox-backup-client binary"
    exit 10
  fi

  if ! [[ -x $(command -v yq) ]]; then
    log_error "FATAL : Missing yq binary (for config parsing)"
    exit 10
  fi

  if ! [[ -x $(command -v jq) ]]; then
    log_error "FATAL : Missing jq binary"
    exit 10
  fi

}

## Actions 
###########################################

function action_backup() {

  readarray -t CONF_ARCHIVES < <(get_archives)
  readonly CONF_ARCHIVES

  log_message "Good day. It's time to do some backups!
Playing configuration called \"$(get_attr name)\" in ${CONFIG_FILE}
We have ${#CONF_ARCHIVES[@]} archives to do. Let's get started"

  log_message_spacer

  # BEGIN Backup start hooks
  # END Backup start hooks

  # BEGIN Loop over all achives

  current_archive=0
  for obj_archive in "${CONF_ARCHIVES[@]}"; do
    ((current_archive++))

    archive=$(get_archive_attr "${obj_archive}" 'archive')
    source=$(get_archive_attr "${obj_archive}" 'source' )
    readarray -t destinations < <(get_archive_attr "${obj_archive}" 'destinations[]')
    readarray -t excludes < <(get_archive_attr "${obj_archive}" 'excludes[]')
    

    log_message "Doing archive ${current_archive}/${#CONF_ARCHIVES[@]} ---- Archive: ${archive}:${source} with ${#destinations[@]} destination(s)"
    #echo "destinations: ${destinations[*]} -- excludes : ${excludes[*]}"

    current_destination=0
    for destination in "${destinations[@]}"; do
      ((current_destination++))
      log_message "Doing destination ${current_destination}/${#destinations[@]}: ${destination}"

      # Validate that the defined destination exists in conf
      if [[ $(get_destination "${destination}") == 'null' ]]; then
        log_error "##### Configuration ERROR: Specified destination does not exists in config file."
        continue;
      fi

      #get_destination_attr "${destination}" 'host'
      destination_password=$(get_destination_attr "${destination}" 'password')
      destination_user=$(get_destination_attr "${destination}" 'user')
      destination_host=$(get_destination_attr "${destination}" 'host')
      destination_datastore=$(get_destination_attr "${destination}" 'datastore')

      ret=$(PBS_PASSWORD="${destination_password}" \
            PBS_REPOSITORY="${destination_user}@${destination_host}:${destination_datastore}" \
            ${PROXMOX_BACKUP_CLIENT_BIN} status 2>&1)
      rc=$?

      if [[ $rc -ne 0 ]]; then
        log_error "##### ERROR - Did not succed to get datastore status"
        log_error "proxmox-bacup-client returned code ${rc} with message: ${ret}"
        continue;
      fi

      exclude_arg=()
      for exclude in "${excludes[@]}"; do
        exclude_arg=("${exclude_arg[@]}" --exclude "${exclude}")
      done


      ret=$(PBS_PASSWORD="${destination_password}" \
            PBS_REPOSITORY="${destination_user}@${destination_host}:${destination_datastore}" \
            ${PROXMOX_BACKUP_CLIENT_BIN} backup "${archive}:${source}" --all-file-systems "${exclude_arg[@]}" 2>&1)
      rc=$?

      if [[ $rc -ne 0 ]]; then
        log_error "##### ERROR during backup ! - Please check!"
        log_error "proxmox-bacup-client returned code ${rc} with message: ${ret}"
        continue;
      fi
      
      
      log_message "Finished doing destination ${current_destination}/${#destinations[@]}: ${destination}"
    done

    log_message "Finished archive ${current_archive}/${#CONF_ARCHIVES[@]} ---- Archive: ${archive}:${source} with ${#destinations[@]} destination(s)"
    log_message_spacer

  done



  # END Loop over all achives

  # BEGIN Backup end hooks
  # END Backup end hooks

}

function action_list() {
  readarray -t CONF_LIST_DESTINATIONS < <(get_destination_list)
  readonly CONF_LIST_DESTINATIONS

  current_destination=0
  for destination in "${CONF_LIST_DESTINATIONS[@]}"; do
    ((current_destination++))

    log_message "Doing destination ${current_destination}/${#CONF_LIST_DESTINATIONS[@]}: ${destination}"


    destination_password=$(get_destination_attr "${destination}" 'password')
    destination_user=$(get_destination_attr "${destination}" 'user')
    destination_host=$(get_destination_attr "${destination}" 'host')
    destination_datastore=$(get_destination_attr "${destination}" 'datastore')

    ret=$(PBS_PASSWORD="${destination_password}" \
          PBS_REPOSITORY="${destination_user}@${destination_host}:${destination_datastore}" \
          ${PROXMOX_BACKUP_CLIENT_BIN} status 2>&1)
    rc=$?

    if [[ $rc -ne 0 ]]; then
      log_error "##### ERROR - Did not succed to get datastore status"
      log_error "proxmox-bacup-client returned code ${rc} with message: ${ret}"
      continue;
    fi

    ret=$(PBS_PASSWORD="${destination_password}" \
          PBS_REPOSITORY="${destination_user}@${destination_host}:${destination_datastore}" \
          ${PROXMOX_BACKUP_CLIENT_BIN} snapshot list "host/$(hostname)" 2>&1)
    rc=$?

    if [[ $rc -ne 0 ]]; then
      log_error "##### ERROR - Did not succed to get datastore status"
      log_error "proxmox-bacup-client returned code ${rc} with message: ${ret}"
      continue;
    else
      echo "${ret}"
    fi

  done
}

function action_mount() {
  echo "action_mount"
}

function action_command() {
  local name_filter=$1

  readarray -t CONF_LIST_DESTINATIONS < <(get_destination_list)
  readonly CONF_LIST_DESTINATIONS



  current_destination=0
  for destination in "${CONF_LIST_DESTINATIONS[@]}"; do
    ((current_destination++))

    log_message "Showing command for destination ${current_destination}/${#CONF_LIST_DESTINATIONS[@]}: ${destination}"


    destination_password=$(get_destination_attr "${destination}" 'password')
    destination_user=$(get_destination_attr "${destination}" 'user')
    destination_host=$(get_destination_attr "${destination}" 'host')
    destination_datastore=$(get_destination_attr "${destination}" 'datastore')

    if [[ "${name_filter}" = "$destination" || "${name_filter}" = "" ]]; then

      cat <<UNLIKELY_EOF
PBS_PASSWORD="${destination_password}" \
PBS_REPOSITORY="${destination_user}@${destination_host}:${destination_datastore}" \
${PROXMOX_BACKUP_CLIENT_BIN} 
UNLIKELY_EOF

    fi

  done
}



## It starts here, for real !
###########################################
###########################################

check_requirements

verbose=0
default_config="ypbcw.yaml"
default_action="backup"


LOGGER_BIN=$(command -v logger)
readonly LOGGER_BIN

PROXMOX_BACKUP_CLIENT_BIN=$(command -v proxmox-backup-client)
readonly PROXMOX_BACKUP_CLIENT_BIN

YQ_BIN=$(command -v yq)
readonly YQ_BIN

JQ_BIN=$(command -v jq)
readonly JQ_BIN


while getopts ":c:a:vh" o; do
  case "${o}" in
    c)
      if [[ -f "${OPTARG}" ]]; then
          arg_config=${OPTARG}
      else 
          echo "Error -- specified config file does not exist"
          exit 1
      fi
      ;;
    a)
      if [[ ! "${OPTARG}" =~ backup|list|mount|command ]]; then
          echo "Incorrect action provided"
          exit 1
      else
        arg_action="${OPTARG}"
      fi
      ;;
    v)
      verbose=1
      ;;
    h)
      help
      ;;
    *)
      usage
      ;;
  esac
done
shift $((OPTIND-1))

CONFIG_FILE=${arg_config:-$default_config}
readonly CONFIG_FILE

ACTION=${arg_action:-$default_action}
readonly ACTION



## Config Validation & Parsing

if ! [[  -f "${CONFIG_FILE}" ]]; then
  log_error "FATAL: Config file does not exist"
  exit 1
fi


if ! ${YQ_BIN} -e . "${CONFIG_FILE}" >/dev/null 2>&1; then
  log_error "FATAL: Config file ${CONFIG_FILE} isn't valid"
  exit 2
fi

FULL_JSON_CONFIG=$(${YQ_BIN} . "${CONFIG_FILE}")
readonly FULL_JSON_CONFIG


case ${ACTION} in
  backup)
    action_backup
  ;;
  list)
    action_list
  ;;
  mount)
    action_mount
  ;;
  command)
    action_command "$1"
  ;;
  *)
    log_error "Error: Invalid action"
  ;;
esac

